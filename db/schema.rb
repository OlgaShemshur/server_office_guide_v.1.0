# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  create_table "companies", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.integer "main_admin_id", null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.datetime "updated_at"
    t.index ["main_admin_id"], name: "fk_main_admin_id"
    t.index ["name"], name: "name", unique: true
  end

  create_table "marks", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.integer "ax", null: false
    t.integer "ay", null: false
    t.string "name", limit: 50, null: false
    t.integer "office_id", null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.datetime "updated_at"
    t.index ["name"], name: "name", unique: true
    t.index ["office_id"], name: "fk_office_id_m"
  end

  create_table "office_tables", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.integer "ax", null: false
    t.integer "ay", null: false
    t.integer "length", null: false
    t.integer "height", null: false
    t.string "side", limit: 1, null: false
    t.integer "office_id", null: false
    t.integer "user_id"
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.datetime "updated_at"
    t.index ["office_id"], name: "fk_office_id_t"
    t.index ["user_id"], name: "fk_user_id_t"
  end

  create_table "offices", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.integer "company_id", null: false
    t.string "created_by", limit: 50
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.datetime "updated_at"
    t.float "lon", limit: 53
    t.float "lat", limit: 53
    t.index ["company_id"], name: "fk_company"
    t.index ["name"], name: "name", unique: true
  end

  create_table "positions", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "name", limit: 50
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.datetime "updated_at"
    t.index ["name"], name: "name", unique: true
  end

  create_table "roles", primary_key: "name", id: :string, limit: 50, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.datetime "updated_at"
    t.index ["name"], name: "name", unique: true
  end

  create_table "team_users", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.integer "team_id", null: false
    t.integer "user_id", null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.datetime "updated_at"
  end

  create_table "teams", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.integer "lead_id"
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.datetime "updated_at"
    t.index ["lead_id"], name: "fk_lead"
    t.index ["name"], name: "name", unique: true
  end

  create_table "user_data", id: :integer, default: nil, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "name", limit: 50
    t.string "surname", limit: 50
    t.integer "position_id"
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.datetime "updated_at"
    t.index ["position_id"], name: "fk_position"
  end

  create_table "users", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "login", limit: 50, null: false
    t.string "password", limit: 50, null: false
    t.string "role", limit: 50, null: false
    t.integer "company_id"
    t.boolean "isactive", default: true, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.datetime "updated_at"
    t.index ["login", "password"], name: "surnameIndex", unique: true
    t.index ["login"], name: "login", unique: true
    t.index ["role"], name: "fk_role"
  end

  create_table "walls", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.integer "ax", null: false
    t.integer "ay", null: false
    t.integer "cx", null: false
    t.integer "cy", null: false
    t.integer "office_id", null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }
    t.datetime "updated_at"
    t.index ["office_id"], name: "fk_office_id"
  end

  create_table "work_statuses", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "workstatus", limit: 1, default: "W", null: false
    t.integer "personal_id", null: false
    t.date "dt", null: false
    t.index ["personal_id"], name: "fk_personal_id"
  end

  add_foreign_key "companies", "users", column: "main_admin_id", name: "fk_main_admin_id"
  add_foreign_key "marks", "offices", name: "fk_office_id_m"
  add_foreign_key "office_tables", "offices", name: "fk_office_id_t"
  add_foreign_key "office_tables", "users", name: "fk_user_id_t"
  add_foreign_key "offices", "companies", name: "fk_company"
  add_foreign_key "teams", "users", column: "lead_id", name: "fk_lead"
  add_foreign_key "user_data", "positions", name: "fk_position"
  add_foreign_key "user_data", "users", column: "id", name: "fk_id"
  add_foreign_key "users", "roles", column: "role", primary_key: "name", name: "fk_role"
  add_foreign_key "walls", "offices", name: "fk_office_id"
  add_foreign_key "work_statuses", "users", column: "personal_id", name: "fk_personal_id"
end
