/**
 * Created by Olga on 16.09.2017.
 */

const backendColor = 'rgba(41, 50, 55, 1)';
const wallColor = 'rgba(225, 225, 225, 1)';
const shadowWallColor = 'rgba(225, 225, 225, 0.5)';
const backLineColor = 'rgba(225, 225, 225, 0.5)';
const backMainLineColor = 'rgba(150, 150, 150, 1)';
const wallLengthColor = 'rgba(0, 119, 225, 1)';
const wallCoordsColor = 'rgba(57, 240, 5, 0.55)';
const wallMarksColor = 'rgba(240, 209, 0, 1)';
const wallTableColor = 'rgba(155, 155, 155, 1)';
const wallTableInnerColor = 'rgba(155, 155, 155, 0.3)';
const TableZoneColor = 'rgba(0, 255, 162, 0.3)';
const errorDrawWallMessageHeader = 'Ошибка! Невозможно отобразить стену: ';
const errorDrawMarkMessageHeader = 'Ошибка! Невозможно отобразить отметку: ';
const errorDrawObjectMessageHeader = 'Ошибка! Невозможно отобразить объект: ';
const errorCrossMessage = 'пересечение с другими объектами';
const errorStartPointNotSelected = 'не выбрана начльная точка';
const errorEndPointNotSelected = 'не выбрана конечная точка';
const errorWallLengthNotSelected = 'не выбрана либо неверно здана длина';
const errorWidthNotSelected = 'не выбрана либо неверно здана ширина';
const errorCrossBoundsMessage = 'точка выходит за рамки ограничения.';
const coordinatesAreChoosenMessage = 'Выбрана точка: ';
const wallIsDeletedMessage = 'Удалена стена: ';
const tableDeletedMessage = 'Удален стол ';
const maxMapLength = 2900;
const maxMapHeight = 2900;
const mapHeight = 3000;
const mapLength = 3000;
const workSpace = 50;
const bounds = 100;

function get_maps_controller() {
    class Map
    {
        constructor(id, mapWrapperId)
        {
            this.id = id;
            this.map = document.getElementById(id);
            this.ctx = this.map.getContext("2d");
            this.walls = [];
            this.marks = [];
            this.tables = [];
            var jqstr = "#" + mapWrapperId;
            if(mapWrapperId == null || '' || $(jqstr).get( 0 ) === undefined) this.mapWrapper = this;
            else this.mapWrapper = $(jqstr).get( 0 );
            this.coordinatesFlag = false;
            this.selectedPointFlag = true;
            this.selectedPoint = null;
            this.selectedEndPoint = null;
            this.entToStartPointmode = false;
            this.doubleClickmode = false;
        }

        newTable(length, height, tableSelect)
        {
            this.addTable(this.selectedPoint, length, height, tableSelect);
        }

        isValidPlaceForTable(selectedPoint, length, height, tableSelect)
        {
            //check selected points
            if(selectedPoint == null) {
                this.consoleOutput(errorDrawObjectMessageHeader + errorStartPointNotSelected);
                return false;
            }
            length = parseInt(length);
            height = parseInt(height);
            var flag = selectedPoint[0] < bounds || selectedPoint[1] < bounds || (selectedPoint[0] + length) > maxMapLength || (selectedPoint[1] + height) > maxMapHeight;
            if (flag) {
                var message =  errorDrawObjectMessageHeader + errorCrossBoundsMessage + 'Координаты: [' + selectedPoint[0] +',' + selectedPoint[1] + ']<br/>' + $('#console').html();
                this.consoleOutput(message);
                return false;
            }
            if(length == null || length <= 0) {
                this.consoleOutput(errorDrawObjectMessageHeader + errorWallLengthNotSelected);
                return false;
            }
            if(height == null || height <= 0) {
                this.consoleOutput(errorDrawObjectMessageHeader + errorWidthNotSelected);
                return false;
            }

            //check with work zone
            var tableArr = [];
            switch(tableSelect) {
                case '0' :
                {
                    flag = (selectedPoint[0]-workSpace) < bounds;
                    tableArr = [[selectedPoint[0]-workSpace, selectedPoint[1]],[selectedPoint[0] + length, selectedPoint[1]],[selectedPoint[0] + length, selectedPoint[1] + height],[selectedPoint[0]-workSpace, selectedPoint[1] + height]];
                    break;
                }
                case '1' :
                {
                    flag = (selectedPoint[0]+length+workSpace) > maxMapLength;
                    tableArr = [[selectedPoint[0], selectedPoint[1]],[selectedPoint[0] + length + workSpace, selectedPoint[1]],[selectedPoint[0] + length + workSpace, selectedPoint[1] + height],[selectedPoint[0], selectedPoint[1] + height]];
                    break;
                }
                case '2' :
                {
                    flag = (selectedPoint[1]-workSpace) < bounds;
                    tableArr = [[selectedPoint[0], selectedPoint[1] - workSpace],[selectedPoint[0] + length, selectedPoint[1] - workSpace],[selectedPoint[0] + length, selectedPoint[1] + height],[selectedPoint[0], selectedPoint[1] + height]];
                    break;
                }
                case '3' :
                {
                    flag = (selectedPoint[1]+height+workSpace) > maxMapHeight;
                    tableArr = [[selectedPoint[0], selectedPoint[1]],[selectedPoint[0] + length, selectedPoint[1]],[selectedPoint[0] + length, selectedPoint[1] + height + workSpace],[selectedPoint[0], selectedPoint[1] + height + workSpace]];
                    break;
                }
            }
            if(flag) {
                var message = errorDrawObjectMessageHeader + errorCrossMessage;
                this.consoleOutput(message);
                return false;
            }

            //check crossing with walls
            var tmp = this;
            var controller = directory_length();
            for(var i=0; i < tableArr.length; i++) {
                var pointSA = tableArr[i];
                var pointEA = [];
                if (i + 1 < tableArr.length) pointEA = tableArr[i + 1];
                else pointEA = tableArr[0];
                for (var a = 0; a < tmp.walls.length; a++) {
                    var pointSB = tmp.walls[a][0];
                    var pointEB = tmp.walls[a][1];
                    var cross = controller.isCross(pointSA, pointEA, pointSB, pointEB);
                    if (cross) {
                        var message = errorDrawObjectMessageHeader + errorCrossMessage;
                        tmp.consoleOutput(message);
                        return false;
                    }
                }

                //check crossing with tables
                for (var b = 0; b < tmp.tables.length; b++) {
                    var tmpArr = [];
                    var tmpPoint = tmp.tables[b][0];
                    var tmplength = tmp.tables[b][1];
                    var tmpheight = tmp.tables[b][2];
                    switch (tmp.tables[b][3]) {
                        case '0' : {
                            flag = (tmpPoint[0]-workSpace) < bounds;
                            tmpArr = [[tmpPoint[0] - workSpace, tmpPoint[1]], [tmpPoint[0] + tmplength, tmpPoint[1]], [tmpPoint[0] + tmplength, tmpPoint[1] + tmpheight], [tmpPoint[0] - workSpace, tmpPoint[1] + tmpheight]];
                            break;
                        }
                        case '1' : {
                            flag = (tmpPoint[0]+tmplength+workSpace) > maxMapLength;
                            tmpArr = [[tmpPoint[0], tmpPoint[1]], [tmpPoint[0] + tmplength + workSpace, tmpPoint[1]], [tmpPoint[0] + tmplength + workSpace, tmpPoint[1] + tmpheight], [tmpPoint[0], tmpPoint[1] + tmpheight]];
                            break;
                        }
                        case '2' : {
                            flag = (tmpPoint[1]-workSpace) < bounds;
                            tmpArr = [[tmpPoint[0], tmpPoint[1] - workSpace], [tmpPoint[0] + tmplength, tmpPoint[1] - workSpace], [tmpPoint[0] + tmplength, tmpPoint[1] + tmpheight], [tmpPoint[0], tmpPoint[1] + tmpheight]];
                            break;
                        }
                        case '3' : {
                            flag = (tmpPoint[1]+tmpheight+workSpace) > maxMapHeight;
                            tmpArr = [[tmpPoint[0], tmpPoint[1]], [tmpPoint[0] + tmplength, tmpPoint[1]], [tmpPoint[0] + tmplength, tmpPoint[1] + tmpheight + workSpace], [tmpPoint[0], tmpPoint[1] + tmpheight + workSpace]];
                            break;
                        }
                    }
                    for (var ba = 0; ba < tmpArr.length; ba++) {
                        var pointSB = tmpArr[ba];
                        var pointEB = [];
                        if (ba + 1 < tmpArr.length) pointEB = tmpArr[ba + 1];
                        else pointEB = tmpArr[0];
                        var cross = controller.isCross(pointSA, pointEA, pointSB, pointEB);
                        if (cross) {
                            var message = errorDrawObjectMessageHeader + errorCrossMessage;
                            tmp.consoleOutput(message);
                            return false;
                        }
                    }
                }
            }

            return !flag;
        }

        addTable(selectedPoint, length, height, tableSelect, attr)
        {
            if (this.isValidPlaceForTable(this.selectedPoint, length, height, tableSelect))
            {
                if(attr == undefined) this.tables.push([selectedPoint,length,height,tableSelect]);
                else if (Number.isInteger(attr))  this.tables.push([selectedPoint,length,height,tableSelect,attr]);
                this.drawTable(selectedPoint,length,height,tableSelect);
            }
        }

        drawTable(selectedPoint, length, height, tableSelect)
        {
            this.ctx.fillStyle = TableZoneColor;
            tableSelect = parseInt(tableSelect);
            length = parseInt(length);
            height = parseInt(height);
            switch(tableSelect) {
                case 0 :
                {
                    this.ctx.fillRect(selectedPoint[0]-workSpace,selectedPoint[1],workSpace,height);
                    break;
                }
                case 1 :
                {
                    this.ctx.fillRect(selectedPoint[0]+length,selectedPoint[1],workSpace,height);
                    break;
                }
                case 2 :
                {
                    this.ctx.fillRect(selectedPoint[0],selectedPoint[1]-workSpace,length,workSpace);
                    break;
                }
                case 3 :
                {
                    this.ctx.fillRect(selectedPoint[0],selectedPoint[1]+height,length,workSpace);
                    break;
                }
            }
            this.ctx.fillStyle = wallTableInnerColor;
            this.ctx.fillRect(selectedPoint[0],selectedPoint[1],length,height);
            this.ctx.strokeStyle = wallTableColor;
            this.ctx.rect(selectedPoint[0],selectedPoint[1],length,height);
            this.ctx.stroke();
        }

        drawMark(name)
        {
            if(this.selectedPoint == null) {
                this.consoleOutput(errorDrawMarkMessageHeader + errorStartPointNotSelected);
                return;
            }
            var flag = this.selectedPoint[0] < bounds || this.selectedPoint[1] < bounds || this.selectedPoint[0] > maxMapLength || this.selectedPoint[1] > maxMapHeight;
            if (flag) {
                var message = errorDrawMarkMessageHeader + errorCrossBoundsMessage + 'Координаты: [' + this.selectedPoint[0] +',' + this.selectedPoint[1] + ']<br/>' + $('#console').html();
                this.consoleOutput(message);
            }
            this.applyMark(this.selectedPoint, name);
        }

        applyMark(selectedPoint, name, attr)
        {
            var tmp = this;
            var  controller = add_mark();
            tmp.ctx.fillStyle = wallMarksColor;
            tmp.ctx.fillText(name, selectedPoint[0], selectedPoint[1]);
            tmp.ctx.fillText('[' + (selectedPoint[0]-bounds) +',' + (selectedPoint[1]-bounds) + ']', selectedPoint[0], selectedPoint[1]+45);
            controller.addMark(selectedPoint);
            if (attr == undefined) tmp.marks.push([selectedPoint, name]);
            else if (Number.isInteger(attr)) tmp.marks.push([selectedPoint, name, attr]);

        }

        changeDoubleClickmode()
        {
            this.doubleClickmode = !this.doubleClickmode;
        }

        changeReverse()
        {
            this.entToStartPointmode = !this.entToStartPointmode;
        }

        drawMap(file)
        {
            this.enableSelect('startPointBoxLabel', 'endPointsBoxLabel');
            this.coordinatesFlag = true;
            this.drawField();
            this.ctx.lineWidth = 1;
            this.deleteShadow();
            this.ctx.font = "10px Arial";
            this.ctx.fillStyle = wallColor;
            this.drawNet();
            this.drawCoordinateLine();
            if (file != null && file != '') this.drawWallsJSON(file);
        }

        toJSON()
        {
            var office = {};
            var walls = [];
            $.each(this.walls, function (i, wall) {
                var hash = {};
                hash['s_x'] = wall[0][0];
                hash['s_y'] = wall[0][1];
                hash['e_x'] = wall[1][0];
                hash['e_y'] = wall[1][1];
                walls.push(hash);
            });
            office['walls'] = walls;
            var tables = [];
            $.each(this.tables, function (i, table) {
                var hash = {}
                hash['s_x'] = table[0][0];
                hash['s_y'] = table[0][1];
                hash['length'] = table[1];
                hash['height'] = table[2];
                hash['side'] = table[3];
                if(table[4] != undefined) hash['id'] = table[4];
                else hash['id'] = 0;
                tables.push(hash);
            });
            office['tables'] = tables;
            var marks = [];
            $.each(this.marks, function (i, mark) {
                var hash = {}
                hash['s_x'] = mark[0][0];
                hash['s_y'] = mark[0][1];
                hash['name'] = mark[1];
                if(mark[2] != undefined) hash['id'] = mark[2];
                else hash['id'] = 0;
                marks.push(hash);
            });
            office['points'] = marks;
            return JSON.stringify({'office' : office});
        }

        drawField()
        {
            var grd=this.ctx.createLinearGradient(0,bounds,0,0);
            grd.addColorStop(0,backendColor);
            grd.addColorStop(1,"white");
            this.ctx.fillStyle = grd;
            this.ctx.fillRect(0, 0, mapHeight, mapLength);
        }

        drawNet()
        {
            for(var i=0; i<30; i++) {
                this.ctx.beginPath();
                if(i>0) {
                    this.ctx.fillText(i-1 + "m", 80, (i*bounds)-20);
                    this.ctx.fillText(i-1 + "m", (i*bounds)-20, 80);
                    this.ctx.fillText(i-1 + "m", mapHeight-bounds, (i*bounds)+20);
                    this.ctx.fillText(i-1 + "m", i*bounds, mapLength-80);
                }
                this.ctx.strokeStyle = backLineColor;
                this.ctx.setLineDash([5, 15]);
                this.ctx.moveTo(bounds*(i+1), 0);
                this.ctx.lineTo(bounds*(i+1), mapHeight);
                this.ctx.moveTo(0, bounds*(i+1));
                this.ctx.lineTo(mapLength, bounds*(i+1));
                this.ctx.stroke();
                this.ctx.closePath();
            }
        }

        drawCoordinateLine()
        {
            this.ctx.beginPath();
            this.ctx.strokeStyle = backMainLineColor;
            this.ctx.lineWidth = 1.5;
            this.ctx.setLineDash([0, 0]);
            this.ctx.moveTo(bounds, 0);
            this.ctx.lineTo(bounds, mapHeight);
            this.ctx.moveTo(mapLength-bounds, 0);
            this.ctx.lineTo(mapLength-bounds, mapHeight);
            this.ctx.moveTo(0, bounds);
            this.ctx.lineTo(mapLength, bounds);
            this.ctx.moveTo(0, mapHeight-bounds);
            this.ctx.lineTo(mapLength, mapHeight-bounds);
            this.ctx.stroke();
        }

        drawWallsSettings()
        {
            this.ctx.lineWidth = 5;
            this.ctx.shadowOffsetX = 2;
            this.ctx.shadowOffsetY = 2;
            this.ctx.shadowBlur = 5;
            this.ctx.strokeStyle = wallColor;
            this.ctx.shadowColor = shadowWallColor;
            this.ctx.font = "15px Arial";
        }

        deleteShadow()
        {
            this.ctx.shadowOffsetX = 0;
            this.ctx.shadowOffsetY = 0;
            this.ctx.shadowBlur = 0;
            this.ctx.shadowColor = null;
        }

        drawWallsJSON(jsonFile)
        {
            var tmp = this;
            var wallsData = jsonFile;
            var office = wallsData.office;
            var wallsArray = office.walls;
            $.each(wallsArray, function (i, wall) {
               tmp.applyWall([wall.ax, wall.ay], [wall.cx, wall.cy]);
            });
            var tablesArray = office.tables;
            $.each(tablesArray, function (i, table) {
                tmp.addTable([table.ax, table.ay], table.length, table.height, table.side, table.id);
            });
            var pointsArray = office.points;
            $.each(pointsArray, function (i, point) {
                tmp.applyMark([point.ax, point.ay], point.name,point.id);
            })
        }

        reDraw()
        {
            this.clear();
            this.drawMap();
            var tmp = this;
            $.each(this.walls, function (i, wall) {
                tmp.applyWall(wall[0], wall[1], true);
            })
            $.each(this.marks, function (i, wall) {
                tmp.applyMark(wall[0], wall[1], true);
            })
            $.each(this.tables, function (i, table) {
                tmp.drawTable(table[0], table[1], table[2], table[3]);
            })
        }

        clear()
        {
            this.ctx.clearRect(0, 0, mapLength, mapHeight);
        }

        arraysEqual(a, b)
        {
            if (a === b) return true;
            if (a == null || b == null) return false;
            if (a.length != b.length) return false;
            for (var i = 0; i < a.length; ++i) {
                if (a[i] !== b[i]) return false;
            }
            return true;
        }

        applyWall(startPoint, endPoint, attr)
        {
            var flag = startPoint[0] < bounds || startPoint[1] < bounds || endPoint[0] < bounds || endPoint[1] < bounds || startPoint[0] > maxMapLength || startPoint[1] > maxMapHeight || endPoint[0] > maxMapLength || endPoint[1] > maxMapHeight;
            if (flag) {
                var message = errorDrawWallMessageHeader + errorCrossBoundsMessage + 'Координаты: [' + startPoint[0] +',' + startPoint[1] + '], [' + endPoint[0] + ',' + endPoint[1] + ']<br/>' + $('#console').html();
                this.consoleOutput(message);
            }
            else {
                if (!flag && attr == undefined) this.walls.push([startPoint, endPoint]);
                this.drawWallsSettings();
                this.ctx.beginPath();
                this.ctx.moveTo(startPoint[0], startPoint[1]);
                this.ctx.lineTo(endPoint[0], endPoint[1]);
                this.ctx.stroke();
                this.deleteShadow();
                this.ctx.fillStyle = wallCoordsColor;
                this.ctx.fillText('[' + (startPoint[0]-bounds) +',' + (startPoint[1]-bounds) + '], [' + (endPoint[0]-bounds) + ',' + (endPoint[1]-bounds) + ']', startPoint[0], startPoint[1]);
                var tmp = this;
                var controller = directory_length();
                var point = controller.directoryMiddlePoint(startPoint, endPoint);
                tmp.ctx.fillStyle = wallLengthColor;
                tmp.ctx.fillText(controller.directoryLength(startPoint, endPoint) + " m", point[0], point[1]);
            }
        }

        selectStartPoint(event)
        {
            if (!this.coordinatesFlag) return;
            map = this;
            var controller = mouse_event();
            controller.init(map);
            var coordinates = controller.getPointOnMove(event);
            if (coordinates != null) {
                map.consoleOutput(coordinatesAreChoosenMessage + 'x:' + coordinates[0] + '; y:' + coordinates[1]);
                if (map.selectedPointFlag) {
                    map.selectedPoint = [coordinates[0] + bounds, coordinates[1] + bounds];
                    $('#startPointBox').html('x:' + coordinates[0] + '<br/>y:' + coordinates[1]);
                }
                else {
                    map.selectedEndPoint = [coordinates[0] + bounds, coordinates[1] + bounds];
                    $('#endPointBox').html('x:' + coordinates[0] + '<br/>y:' + coordinates[1]);
                }
            }
        }

        coordinatesBoxOutput(event)
        {
            if (!this.coordinatesFlag) return;
            map = this;
            var controller = mouse_event();
            controller.init(map);
            var coordinates = controller.getPointOnMove(event);
            if (coordinates != null) $('#coordinatesBox').html('x:' + coordinates[0] + '<br/>y:' + coordinates[1]);
        }

        consoleOutput(tmp)
        {
            var html = $('#console').html();
            var date = new Date($.now());
            var ms = date.getMilliseconds().toString();
            if (ms.length > 2) ms = ms.substring(0, 2);
            var dateString = date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate() + ' (' + date.getHours() + ':' + date.getMinutes() + ':' + ms + ')';
            var message =  dateString + ' : ' + tmp;
            $('#console').html(message + '<br/>' + html);
        }

        closeMap()
        {
            this.walls = [];
            this.marks = [];
            this.tables = [];
            this.coordinatesFlag = false;
            this.clear();
        }

        drawWall(length, degree)
        {
            var tmp = this;
            if(this.selectedPoint == null) {
                this.consoleOutput(errorDrawWallMessageHeader + errorStartPointNotSelected);
                return;
            }
            if(length == null || length <= 0) {
                this.consoleOutput(errorDrawWallMessageHeader + errorWallLengthNotSelected);
                return;
            }
            if (degree == null) degree = 0;
            var controller = directory_length();
              var endPoint = controller.countCoordinates(tmp.selectedPoint, length, degree);
              tmp.selectedEndPoint = endPoint;
              tmp.applyWall(tmp.selectedPoint, endPoint);
            this.reverseMode();
        }

        deleteWall()
        {
            var tmp = this;
            var controller = directory_length();
            var indexArr = controller.isOnLine(tmp.selectedPoint, tmp.walls);
            if (indexArr.length > 0) {
                for (var i = indexArr.length -1; i >= 0; i--) {
                    tmp.consoleOutput(wallIsDeletedMessage + '[' + (tmp.walls[i][0][0]-bounds) + ',' + (tmp.walls[i][0][1]-bounds) + '],[' + (tmp.walls[i][1][0]-bounds) + ',' + (tmp.walls[i][1][1]-bounds) + ']');
                    tmp.walls.splice(indexArr[i], 1);
                }
                tmp.reDraw();
            }
        }

        moveTable()
        {
            var controller = directory_length();
            for (var index = 0; index < this.tables.length; index++) {
                if(controller.isInSquare(this.selectedPoint, this.tables[index][0], [this.tables[index][0][0]+this.tables[index][1], this.tables[index][0][1]+this.tables[index][2]])) {
                    if (this.isValidPlaceForTable(this.selectedEndPoint, this.tables[index][1], this.tables[index][2], this.tables[index][3])) this.tables[index][0] = this.selectedEndPoint;
                }
            }
            this.reDraw();
            this.reverseMode();
        }

        rotareTable(workZone)
        {
            var controller = directory_length();
            for (var index = 0; index < this.tables.length; index++) {
                if(controller.isInSquare(this.selectedPoint, this.tables[index][0], [this.tables[index][0][0]+this.tables[index][1], this.tables[index][0][1]+this.tables[index][2]])) {
                    if (this.isValidPlaceForTable(this.tables[index][0], this.tables[index][1], this.tables[index][2], workZone)) this.tables[index][2] = workZone;
                }
            }
            this.reDraw();
            this.reverseMode();
        }

        isInSquare(point, pointS, pointE)
        {
            return ((point[0] > pointS[0]) && (point[0] < pointE[0]) && (point[1] > pointS[1]) && (point[1] < pointE[1]));
        }

        deleteTable()
        {
            if(this.selectedPoint == null) {
                this.consoleOutput(errorDrawTableMessageHeader + errorStartPointNotSelected);
                return;
            }
            var tmp = this;
            var controller = directory_length();
            for (var i = tmp.tables.length -1; i >= 0; i--) {
                if(controller.isInSquare(tmp.selectedPoint, tmp.tables[i][0], [tmp.tables[i][0][0]+tmp.tables[i][1], tmp.tables[i][0][1]+tmp.tables[i][2]])) {
                    tmp.consoleOutput(tableDeletedMessage);
                    tmp.tables.splice(i, 1);
                }
            }
            tmp.reDraw();
        }

        deleteMark()
        {
            if(this.selectedPoint == null) {
                this.consoleOutput(errorDrawMarkMessageHeader + errorStartPointNotSelected);
                return;
            }
            for (var i = this.marks.length -1; i >= 0; i--) {
                if(this.selectedPoint[0] == this.marks[i][0][0] && this.selectedPoint[1] == this.marks[i][0][1]) this.marks.splice(i, 1);
            }
            this.reDraw();
        }

        moveMark()
        {
            if(this.selectedPoint == null) {
                this.consoleOutput(errorDrawMarkMessageHeader + errorStartPointNotSelected);
                return;
            }
            if(this.selectedEndPoint == null) {
                this.consoleOutput(errorDrawMarkMessageHeader + errorEndPointNotSelected);
                return;
            }
            var flag = this.selectedPoint[0] < bounds || this.selectedPoint[1] < bounds || this.selectedPoint[0] > maxMapLength || this.selectedPoint[1] > maxMapHeight;
            if (flag) {
                var message = errorDrawMarkMessageHeader + errorCrossBoundsMessage + 'Координаты: [' + this.selectedPoint[0] +',' + this.selectedPoint[1] + ']<br/>' + $('#console').html();
                this.consoleOutput(message);
            }
            for (var i = this.marks.length -1; i >= 0; i--) {
                if(this.selectedPoint[0] == this.marks[i][0][0] && this.selectedPoint[1] == this.marks[i][0][1]) {
                    this.marks[i][0] = this.selectedEndPoint;
                    this.marks[i][0] = this.selectedEndPoint;
                }
            }
            this.reDraw();
            this.reverseMode();
        }

        drawWallBy2points()
        {
            var tmp = this;
            if(this.selectedPoint == null) {
                this.consoleOutput(errorDrawWallMessageHeader + errorStartPointNotSelected);
                return;
            }
            if(this.selectedEndPoint == null) {
                this.consoleOutput(errorDrawWallMessageHeader + errorEndPointNotSelected);
                return;
            }
            tmp.applyWall(tmp.selectedPoint, tmp.selectedEndPoint);
            this.reverseMode();
        }

        reverseMode()
        {
            if(this.entToStartPointmode) {
                var buffer = this.selectedPoint;
                this.selectedPoint = this.selectedEndPoint;
                this.selectedEndPoint = buffer;
                $('#startPointBox').html('x:' + (this.selectedPoint[0]-bounds) + '<br/>y:' + (this.selectedPoint[1]-bounds));
            }
        }

        enableSelect(id, id2)
        {
            this.selectedPointFlag = false;
            var jqstr = "#" + id;
            var jqstr2 = "#" + id2;
            $(jqstr).get(0).style.backgroundColor = 'purple';
            $(jqstr).get(0).style.color = 'white';
            $(jqstr2).get(0).style.backgroundColor =  null;
            $(jqstr2).get(0).style.color =  null;
        }
    }

    map = new Map('map', 'mapWrapper');
    return map;
}

