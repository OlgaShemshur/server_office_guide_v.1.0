/**
 * Created by Olga on 08.10.2017.
 */
function mouse_event() {
    return controller = {
        init: function(map){
            this.map = map;
        },

        getCoordinates: function(e) {
            var map = this.map.map;
            var mapWrapper = this.map.mapWrapper;
            var x;
            var y;
            if (e.pageX || e.pageY) {
                x = e.pageX;
                y = e.pageY;
            }
            else return null;
            x = x - map.offsetLeft + mapWrapper.scrollLeft;
            y = y - map.offsetTop + mapWrapper.scrollTop;
            return [x,y]
        },

        getPointOnMove: function(event)
        {
            var tmpPoint = this.getCoordinates(event);
            if (tmpPoint == null) return tmpPoint;
            var x = Math.round((tmpPoint[0] - 100)/10, 1) * 10;
            var y = Math.round((tmpPoint[1] - 100)/10, 1) * 10;
            if (x < 0) x = 0;
            if (y < 0) y = 0;
            var point = [x, y];
            return point;
        }
    }
}