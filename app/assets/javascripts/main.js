//controller for map (functionality)
controller = {
    map: null,
	run: function(parametr, attchmts){
		if(this.map == null) this.map = get_maps_controller();
		switch(parametr)
		{
		    //creates a new map
			case 'newMap':
			{
				map.clear();
				map.drawMap();
				break;
			}
			//open the saved map
			case 'openMap':
			{
				map.closeMap();
                $.ajax({
                    type: "POST",
                    url: "open_map",
                    data: "id="+attchmts,
                    success: function(msg){
                        map.drawMap(msg);
                        $('#current_office_id').text(attchmts);
                    },
                    error: function(msg){
                        alert(msg);
                    }
                });
				break;
			}
			//select point 'A'
			case 'selectStartPoint':
			{
				map.selectStartPoint(attchmts);
				break;
			}
			//gets coordinates from map block on 'mousemove' action
			case 'getPointOnMove':
			{
				map.coordinatesBoxOutput(attchmts);
				break;
			}
			//cleans current view
			case 'closeMap':
			{
                sessionStorage.removeItem('current_office_id');
				map.closeMap();
				break;
			}
			//activates an mode of point choose by double click
			case 'dClickMode':
			{
				if(!map.doubleClickmode) break;
			}
			//represents walls and adds data
			case 'drawWall':
			{
				if(attchmts != undefined) {
					if(attchmts == 0) {
						var length = document.getElementById('wallLength').value;
						var degree = document.getElementById('wallDegree').value;
						map.drawWall(length, degree);
					}
					if(attchmts == 1) map.drawWallBy2points();
				}
				break;
			}
			//enables to choose point
			case 'enableSelect':
			{
				map.enableSelect(attchmts[0], attchmts[1]);
				break;
			}
			//reverse an value of point choose by double click mode
			case 'changeDClickMode':
			{
				map.changeDoubleClickmode();
				break;
			}
            //reverse an value of reverse  mode
			case 'changeReverse':
			{
				map.changeReverse();
				break;
			}
			//deletes wall by selected point
			case 'deleteWall':
			{
				map.deleteWall();
				break;
			}
			//represents and adds map mark
			case 'drawMark':
			{
				map.drawMark(attchmts);
				break;
			}
			//deletes map mark
			case 'deleteMark':
			{
				map.deleteMark();
				break;
			}
			//moves map mark
			case 'moveMark':
			{
				map.moveMark();
				break;
			}
			case 'addTable':
			{
				var length = document.getElementById('tableLength').value;
				var height = document.getElementById('tableHeight').value;
				var tableSelect = document.getElementById('tableSelect').options[document.getElementById('tableSelect').selectedIndex].value;
				map.newTable(length, height, tableSelect);
				break;
			}
			case 'deleteTable':
			{
				map.deleteTable();
				break;
			}
			case 'moveTable':
			{
				map.moveTable();
				break;
			}
			case 'reverseTable':
			{
				map.rotareTable();
				break;
			}
			case 'saveMap':
			{
				json =  map.toJSON();
                $('<input />').attr('type', 'hidden')
                    .attr('name', 'map_JSON')
                    .attr('value', json)
                    .appendTo('#save_map');
                $( "#save_map" ).submit().success(alert(msg));
				break;
			}
			default :
			{
                sessionStorage.removeItem('current_office_id');
				map.clear();
				map.drawMap();
				break;
			}
		}

	}
};


//controller for dynamic creation of map elements
dhtmlController = {
	run: function(parametr){
		controller = dhtml_for_map();
		switch(parametr)
		{
			case 'addLineMenu':
			{
				controller.addLineMenu();
				break;
			}
			case 'appendAddWall':
			{
				controller.appendAddWall();
				break;
			}
			case 'appendAddWallBy2Points':
			{
				controller.appendAddWallBy2Points();
				break;
			}
			case 'changeReverseImg':
			{
				controller.changeReverseImg();
				break;
			}
			case 'addMark':
			{
				controller.askName();
				break;
			}
			case 'addTable':
			{
				controller.addTable();
				break;
			}
			case 'changeDClickMode':
			{
				controller.changeDClickMode();
				break;
			}
		}

	}
};

//definition of controllers
controllerObject = controller;
dhtmlControllerObject = dhtmlController;

//function for use of controller object
function useController(parametr, attchmts){
    controllerObject.run(parametr, attchmts);
}

//function for use of DHTML controller object
function useDHTMLController(parametr){
    dhtmlControllerObject.run(parametr);
}

//display element by id, sets opacity to 1
function display(id) {
    document.getElementById(id).style.opacity = 1;
}

//hides element by id, sets opacity to 0
function hide(element) {
    element.style.opacity = 0;
}

//displays element
function on(id) {
    document.getElementById(id).style.display = "block";
}

//delete element from page layout
function off(id) {
    document.getElementById(id).style.display = "none";
}

