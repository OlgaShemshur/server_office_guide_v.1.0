/**
 * Created by Olga on 16.09.2017.
 */
function directory_length(){
    return  controller = {
        directoryLength: function(a, b) {
            return Number(((Math.sqrt(Math.pow(a[0] - b[0], 2) + Math.pow(a[1] - b[1], 2))) / 100).toFixed(1))
        },

        directoryMiddlePoint: function(startPoint, endPoint) {
            var x = (Math.abs(startPoint[0] - endPoint[0]))/2;
            var y = (Math.abs(startPoint[1] - endPoint[1]))/2;
            if(startPoint[0] < endPoint[0]) x += startPoint[0];
            else x = startPoint[0] - x;
            if(startPoint[1] < endPoint[1]) y += startPoint[1];
            else y = startPoint[1] - y;
            return [x,y]
        },

        countCoordinates: function(startPoint, length, degree){
        var delimetr = 180/degree;
        var degreeRAD = Math.PI/delimetr;
        y_side = length * Math.cos(degreeRAD);
        x_side = length * Math.sin(degreeRAD);
        var x_e = Math.round((startPoint[0] + x_side)/10, 1) * 10;
        var y_e = Math.round((startPoint[1] + y_side)/10, 1) * 10;
        return [x_e, y_e]
        },

        isInSquare: function(point, pointS, pointE) {
            return ((point[0] > pointS[0]) && (point[0] < pointE[0]) && (point[1] > pointS[1]) && (point[1] < pointE[1]));
        },

        isOnLine: function(point, walls) {
            var indexes = [];
            for (var i = 0; i < walls.length; i++) {
                    if(((point[0] - walls[i][0][0])*(walls[i][1][1] - walls[i][0][1])) == ((point[1] - walls[i][0][1])*(walls[i][1][0] - walls[i][0][0]))) {
                    indexes.push(i);
                }
            }
            return indexes;
        },

        intersect_1: function(a, b, c, d) {
            if (a > b) {
                var z = a;
                a = b;
                b - z;

            }
            if (c > d) {
                var z = c;
                c = d;
                d - z;

            }
            return Math.max(a,c) <= Math.min(b,d);
        },

        det: function(a, b, c, d) {
            return a * d - b * c;
        },

        between: function(a, b, c) {
            var EPS = 1E-9;
            return Math.min(a,b) <= c + EPS && c <= Math.max(a,b) + EPS;
        },

        isCross: function(a, b, c, d) {
            var A1 = a[1]-b[1];
            var B1 = b[0]-a[0];
            var C1 = -A1*a[0] - B1*a[1];
            var A2 = c[1]-d[1];
            var B2 = d[0]-c[0];
            var C2 = -A2*c[0] - B2*c[1];
            var zn = this.det(A1, B1, A2, B2);
            if (zn != 0) {
                var x = -this.det(C1, B1, C2, B2) * 1. / zn;
                var y = -this.det (A1, C1, A2, C2) * 1. / zn;
                return this.between(a[0], b[0], x) && this.between(a[1], b[1], y)
                    && this.between(c[0], d[0], x) && this.between(c[1], d[1], y);
            }
            else
                return this.det(A1, C1, A2, C2) == 0 && this.det(B1, C1, B2, C2) == 0
                    && this.intersect_1(a[0], b[0], c[0], d[0])
                    && this.intersect_1(a[1], b[1], c[1], d[1]);
        }
    }
}