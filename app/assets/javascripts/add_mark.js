/**
 * Created by Olga on 26.11.2017.
 */
function add_mark() {
    return  controller = {
        addMark: function(point)
        {
            var canvas = document.getElementById('map')
            context = canvas.getContext('2d');
            var base_image = new Image(50, 50);
            base_image.src = '../img/star_map.png';
            base_image.width = 50;
            base_image.onload = function () {
                context.drawImage(base_image, point[0], point[1]);
            }
        }
    }
}