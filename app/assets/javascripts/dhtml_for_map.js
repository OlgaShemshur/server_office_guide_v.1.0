/**
 * Created by Olga on 29.10.2017.
 */
function dhtml_for_map() {
    return  controller = {

        changeReverseImg: function() {
            var element = document.getElementById('reverseIMG');
            var scrOff = "../img/refresh.png";
            var srcOn = "../img/refreshOn.png";
            var arr = scrOff.split('/');
            var templateOff = arr[arr.length - 1];
            var srcTmp = element.src;
            if(srcTmp.indexOf(templateOff) > -1)  element.src = srcOn;
            else element.src = scrOff;
        },

        changeDClickMode: function() {
            var element = document.getElementById('doubleClickMode');
            var scrOff = "../img/double_click.png";
            var srcOn = "../img/double_clickOn.png";
            var arr = scrOff.split('/');
            var templateOff = arr[arr.length - 1];
            var srcTmp = element.src;
            if(srcTmp.indexOf(templateOff) > -1)  element.src = srcOn;
            else element.src = scrOff;
        },

        appendAddWall: function() {
            var line0 = document.createElement('div');
            var headerText0 = document.createElement('span');
            headerText0.innerHTML = 'Добавление стены';
            line0.appendChild(headerText0);

            var line1 = document.createElement('div');
            var sizeText = document.createElement('span');
            sizeText.innerHTML = 'Размер стены<br/>';
            var sizeInput = document.createElement('input');
            sizeInput.type = "number";
            sizeInput.id="wallLength";
            sizeInput.style.position="relative";
            line1.appendChild(sizeText);
            line1.appendChild(sizeInput);

            var line2 = document.createElement('div');
            var degreeText = document.createElement('span');
            degreeText.innerHTML = 'Градус<br/>';
            var degreeInput = document.createElement('input');
            degreeInput.style.position="relative";
            degreeInput.type = "number";
            degreeInput.id="wallDegree";
            line2.appendChild(degreeText);
            line2.appendChild(degreeInput);

            var line3 = document.createElement('div');
            var button = document.createElement('button');
            var textNode = document.createTextNode('Добавить');
            button.appendChild(textNode);
            button.addEventListener('click', function(evt) {
                useController('drawWall', 0);
            }, false);
            line3.appendChild(button);
            var myNode = document.getElementById("addBox");
            while (myNode.firstChild) {
                myNode.removeChild(myNode.firstChild);
            }
            document.getElementById('addBox').appendChild(line0);
            document.getElementById('addBox').appendChild(line1);
            document.getElementById('addBox').appendChild(line2);
            document.getElementById('addBox').appendChild(line3);
        },

        appendAddWallBy2Points: function() {
            var line0 = document.createElement('div');
            var headerText0 = document.createElement('span');
            headerText0.innerHTML = 'Добавление стены по 2-м точкам';
            line0.appendChild(headerText0);

            var line1 = document.createElement('div');
            var button = document.createElement('button');
            button.style.marginTop = '20px';
            var textNode = document.createTextNode('Добавить');
            button.appendChild(textNode);
            button.addEventListener('click', function(evt) {
                useController('drawWall', 1);
            }, false);
            line1.appendChild(button);
            var myNode = document.getElementById("addBox");
            while (myNode.firstChild) {
                myNode.removeChild(myNode.firstChild);
            }
            document.getElementById('addBox').appendChild(line0);
            document.getElementById('addBox').appendChild(line1);
        },

        askName: function() {
            var line0 = document.createElement('div');
            var headerText0 = document.createElement('span');
            headerText0.innerHTML = 'Добавление заметки';
            line0.appendChild(headerText0);

            var line1 = document.createElement('div');
            var headerText = document.createElement('span');
            headerText.innerHTML = 'Название';
            line1.style.textAlign =  'left';
            var text = document.createElement('input');
            text.type = 'text';
            text.width = 50;
            text.style.left = '70px';
            text.id = 'markName';
            line1.appendChild(headerText);
            line1.appendChild(text);

            var line2 = document.createElement('div');
            var button = document.createElement('button');
            var textNode = document.createTextNode('Применить');
            button.appendChild(textNode);
            button.width = 70;
            button.addEventListener('click', function(evt) {
                useController('drawMark', document.getElementById('markName').value);
            }, false);
            var myNode = document.getElementById("addBox");
            while (myNode.firstChild) {
                myNode.removeChild(myNode.firstChild);
            }
            line2.appendChild(button);

            document.getElementById('addBox').appendChild(line0);
            document.getElementById('addBox').appendChild(line1);
            document.getElementById('addBox').appendChild(line2);
        },

        addLineMenu: function() {
            var line0 = document.createElement('div');
            var headerText0 = document.createElement('span');
            headerText0.innerHTML = 'Добавление стены';
            line0.appendChild(headerText0);

            var line1 = document.createElement('div');
            var button0 = document.createElement('button');
            var textNode0 = document.createTextNode('По известной длине и углу');
            button0.appendChild(textNode0);
            button0.style.width = '90%';
            button0.style.height = '30px';
            line1.style.textAlign = 'center';
            button0.addEventListener('click', function(evt) {
                useDHTMLController('appendAddWall');
            }, false);
            line1.appendChild(button0);

            var line2 = document.createElement('div');
            var button1 = document.createElement('button');
            var textNode1 = document.createTextNode('По 2-м точкам');
            button1.appendChild(textNode1);
            button1.style.width = '90%';
            button1.style.height = '30px';
            button1.style.marginTop = '10px';
            button1.addEventListener('click', function(evt) {
                useDHTMLController('appendAddWallBy2Points');
            }, false);
            line2.appendChild(button1);
            var myNode = document.getElementById("addBox");
            while (myNode.firstChild) {
                myNode.removeChild(myNode.firstChild);
            }

            document.getElementById('addBox').appendChild(line0);
            document.getElementById('addBox').appendChild(line1);
            document.getElementById('addBox').appendChild(line2);
        },

        addTable: function() {
            var line0 = document.createElement('div');
            var headerText0 = document.createElement('span');
            headerText0.innerHTML = 'Добавление стола';
            line0.appendChild(headerText0);

            var line1 = document.createElement('div');
            var headerText = document.createElement('span');
            headerText.innerHTML = 'Ширина';
            var length = document.createElement('input');
            length.type = 'number';
            length.width = 100;
            length.id = 'tableLength';
            line1.style.textAlign =  'left';
            line1.appendChild(headerText);
            line1.appendChild(length);

            var line2 = document.createElement('div');
            var headerTextH = document.createElement('span');
            headerTextH.innerHTML = 'Длина';
            var heigth = document.createElement('input');
            heigth.type = 'number';
            heigth.width = 100;
            heigth.id = 'tableHeight';
            line2.style.textAlign =  'left';
            line2.appendChild(headerTextH);
            line2.appendChild(heigth);

            var line3 = document.createElement('div');
            var headerTextO = document.createElement('span');
            headerTextO.innerHTML = 'Рабочя сторона';
            var select = document.createElement('select');
            select.id = 'tableSelect';
            var option1 = document.createElement('option');
            option1.value = '0';
            var option1textNode = document.createTextNode('←');
            option1.appendChild(option1textNode);
            var option2 = document.createElement('option');
            option2.value = '1';
            var option2textNode = document.createTextNode('→');
            option2.appendChild(option2textNode);
            var option3 = document.createElement('option');
            option3.value = '2';
            var option3textNode = document.createTextNode('↑');
            option3.appendChild(option3textNode);
            var option4 = document.createElement('option');
            option4.value = '3';
            var option4textNode = document.createTextNode('↓');
            option4.appendChild(option4textNode);
            select.appendChild(option1);
            select.appendChild(option2);
            select.appendChild(option3);
            select.appendChild(option4);
            line3.appendChild(headerTextO);
            line3.appendChild(select);
            line3.style.textAlign =  'left';

            var line4 = document.createElement('div');
            var button = document.createElement('button');
            var textNode = document.createTextNode('Применить');
            button.appendChild(textNode);
            button.width = 70;
            button.addEventListener('click', function(evt) {
                useController('addTable');
            }, false);
            line4.appendChild(button);

            var myNode = document.getElementById("addBox");
            while (myNode.firstChild) {
                myNode.removeChild(myNode.firstChild);
            }
            document.getElementById('addBox').appendChild(line0);
            document.getElementById('addBox').appendChild(line1);
            document.getElementById('addBox').appendChild(line2);
            document.getElementById('addBox').appendChild(line3);
            document.getElementById('addBox').appendChild(line4);
        }
    }
}
