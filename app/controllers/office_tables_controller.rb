class OfficeTablesController < ApplicationController
  before_action :set_office_table, only: [:show, :edit, :update, :destroy]

  # GET /office_tables
  # GET /office_tables.json
  def index
    @office_tables = OfficeTable.all
  end

  # GET /office_tables/1
  # GET /office_tables/1.json
  def show
  end

  # GET /office_tables/new
  def new
    @office_table = OfficeTable.new
  end

  # GET /office_tables/1/edit
  def edit
  end

  # POST /office_tables
  # POST /office_tables.json
  def create
    @office_table = OfficeTable.new(office_table_params)

    respond_to do |format|
      if @office_table.save
        format.html { redirect_to @office_table, notice: 'Office table was successfully created.' }
        format.json { render :show, status: :created, location: @office_table }
      else
        format.html { render :new }
        format.json { render json: @office_table.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /office_tables/1
  # PATCH/PUT /office_tables/1.json
  def update
    respond_to do |format|
      if @office_table.update(office_table_params)
        format.html { redirect_to @office_table, notice: 'Office table was successfully updated.' }
        format.json { render :show, status: :ok, location: @office_table }
      else
        format.html { render :edit }
        format.json { render json: @office_table.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /office_tables/1
  # DELETE /office_tables/1.json
  def destroy
    @office_table.destroy
    respond_to do |format|
      format.html { redirect_to office_tables_url, notice: 'Office table was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_office_table
      @office_table = OfficeTable.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def office_table_params
      params.require(:office_table).permit(:title, :body)
    end
end
