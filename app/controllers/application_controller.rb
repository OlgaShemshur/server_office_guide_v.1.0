class ApplicationController < ActionController::Base

  def exit
    session[:current_user_id] = nil
    render template: "pages/index"
  end

  def index
    if session[:current_user_id]
      begin
        current_user = User.find_by(id: session[:current_user_id])
        unless current_user
          session[:current_user_id] = nil
          self.index
        else
          case current_user.role
          when 'remote_admin'
            @companies = Company.all
            @admins = {}
            @companies.each do |company|
              @admins[company.id] = User.find_by(id: company.main_admin_id)
            end
          when 'local_admin'
            @users = User.where("company_id = ?", current_user.company_id)
            @user_data = {}
            @users.each do |user|
              @user_data[user.id] = UserDatum.find_by(id: user.id)
            end
          when 'editor'
            @add = false
            @open = false
            @offices ||= Office.where("company_id = ?", current_user.company_id)
            @offices||=[]
            @users = User.where("company_id = ?", current_user.company_id)
            @user_data = {}
            @users.each do |user|
              @user_data[user.id] = UserDatum.find_by(id: user.id)
            end
          when 'hr'
            @users = User.where("company_id = ?", current_user.company_id)
            @user_data = {}
            @workstatuses = {}
            back_status_hash = {
                'w'=>'1',
                'i'=>'2',
                'v'=>'3',
                'u'=>'4',
                'o'=>'5'
            }
            workstatuses_arr = [['работает', 1], ['болен', 2], ['отпуск', 3],['отгул', 4], ['прочее', 5]]
            @users.each do |user|
              @user_data[user.id] = UserDatum.find_by(id: user.id)
              if @user_data[user.id]['position_id']
                @user_data["#{user.id}_p"] ||= ''
                @user_data["#{user.id}_p"] = (Position.find_by_id(@user_data[user.id]['position_id'])).name
              end
              tmp = WorkStatus.find_by("personal_id = ? and dt = CURDATE()", user.id) ? WorkStatus.find_by("personal_id = ? and dt = CURDATE()", user.id).workstatus : 'w'
              workstatus_tmp = back_status_hash[tmp]
              @workstatuses[user.id] ||= []
              workstatuses_arr.each do |workstatus|
                if workstatus[1] == workstatus_tmp.to_i
                  @workstatuses[user.id].unshift workstatus
                else
                  @workstatuses[user.id]. push workstatus
                end
              end
            end
          end
          session[:current_office_id] = nil
          session[:current_office_name] = nil
          render template: "#{current_user.role}"
        end
      rescue
        session[:current_user_id] = nil
        self.index
      end
    else
      render template: "pages/index"
    end
  end

  def main
    begin
      render template: "pages/#{params[:page]}"
    rescue
      self.index
    end
  end

  def css
    render template: "css/#{params[:page]}"
  end

  def js
    file_name =  ''
    file_name =  "/#{params[:page]}.js" if File.exist? "app/views/js/#{params[:page]}.js"
    file_name =  "/lib/#{params[:page]}.js" if File.exist? "app/views/js/lib/#{params[:page]}.js"
    render template: "js/#{file_name}"
  end

  def img
    file_name =  "app/views/img/#{params[:page]}.png"
    send_data open(file_name, "rb") { |f| f.read }
  end

  def fonts
    render template: "fonts/#{params[:page]}"
  end

  def current_user
    @_current_user ||= session[:current_user_id] &&
        User.find_by(id: session[:current_user_id])
  end

  def login
    begin
      params[:login].encode("UTF-8")
      params[:pwd].encode("UTF-8")
      current_user = User.find_by(login: params[:login])
      @islogin = 0 unless current_user
      @islogin = 1 unless params[:login].match (/[A-z0-9_]{6,}/)
      @ispwd = 1 unless params[:pwd].match (/[A-z0-9_]{6,}/)
      @ispwd = 2 if params[:pwd].empty?
      @islogin = 2 if params[:login].empty?
      current_user = User.find_by(login: params[:login], password: params[:pwd])
      current_user ? @_current_user ||= true : @_current_user ||= false
      session[:current_user_id] = current_user.id if current_user
      unless current_user
        @ispwd = 0
        @ispwd = 2 if params[:pwd].empty?
      end
    rescue
      @islogin = 1
      @ispwd = 0
    end
    self.index
  end

  def company_registration
    @success = 0
    @iscompany = 2 if params[:name].empty?
    isvalid = params[:name].match (/[A-z0-9_]{6,}/)
    @iscompany = 1 unless isvalid
    company = nil
    isvalid ? company = Company.find_by(name: params[:name]) : (render template: "registration.erb")
    company ? @iscompany = 0 : status = true
    if status && self.registration(params[:login], params[:pwd], 'local_admin')
      current_user = User.find_by(login: params[:login], password: params[:pwd])
      company = Company.new(name: params[:name], main_admin_id: current_user.id)
      company.save ? @success = 1 : @success = 0
      current_user.update(company_id: company[:id])
    end
    render template: "pages/registration.erb"
  end

  def registration(new_login, new_pwd, role)
    status = {login: false, pwd: false}
    current_user = User.find_by(login: new_login)
    current_user ? @islogin = 0 : status[:login] = true
    @islogin = 2 if new_login.empty?
    if new_login.match (/[A-z0-9_]{6,}/)
      status[:login] = true
    else
      @islogin = 1
      status[:login] = false
    end
    if new_pwd.match (/[A-z0-9_]{6,}/)
      status[:pwd] = true
    else
      status[:pwd] = false
      @ispwd = 1
    end
    @ispwd = 2 if new_pwd.empty?
    current_user = User.find_by(login: new_login, password: new_pwd)
    current_user ? @_current_user ||= true : @_current_user ||= false
    if status[:login] && status[:pwd]
      user = User.new(login: new_login, password: new_pwd, role: role)
      user.save
      user = UserDatum.new(id: user.id)
      user.save
    else
      false
    end
  end

  def company_manage
    begin
      company = Company.find_by(id: params[:company].keys[0])
      current_user = User.find_by(id: company.main_admin_id)
      current_user.update(login: params[:manage][:login], password: params[:manage][:pwd])
      company.update(name: params[:manage][:name])
      @success = 1
    rescue
      @success = 0
    end
    self.index
  end

  def search_company
    if session[:current_user_id]
      current_user = User.find_by(id: session[:current_user_id])
      company = Company.find_by(name: params[:search_company][:name])
      @companies = []
      @companies.push company if company
      @admins = {}
      @companies.each do |company|
        @admins[company.id] = User.find_by(id: company.main_admin_id)
      end
      render template: "#{current_user.role}"
    else
      render template: "pages/index"
    end
  end

  def user_manage
    begin
      current_user = User.find_by(id: params[:user].keys[0])
      current_user.update(login: params[:manage_user][:login], password: params[:manage_user][:pwd])
      @success = 1
    rescue
      @success = 0
    end
    self.index
  end

  def search_user
    if session[:current_user_id]
      current_user = User.find_by(id: session[:current_user_id])
      user = User.find_by(login: params[:search_user][:login])
      @users = []
      @users.push user if user
      @user_data = {}
      @workstatuses= {}
      @users.each do |user|
        @user_data[user.id] = UserDatum.find_by(id: user.id)
        @workstatuses[user.id] = WorkStatus.find_by("personal_id = ? and dt = CURDATE()", user.id)
      end
      render template: "#{current_user.role}"
    else
      render template: "pages/index"
    end
  end

  def add_user
    roles = Role.all
    role_values = []
    roles.each do |role|
      role_values.push role.name
    end
    role_values.delete 'remote_admin'
    current_user = User.find_by(id: session[:current_user_id])
    role_values.include?(params[:add_user][:role]) ? (self.registration(params[:add_user][:login], params[:add_user][:pwd], params[:add_user][:role])) : @success = 0
    new_user = User.find_by(login: params[:add_user][:login], password: params[:add_user][:pwd])
    new_user.update(company_id: current_user.company_id) if new_user
    self.index
  end

  def save_map
    if session[:current_user_id]
      current_user = User.find_by(id: session[:current_user_id])
      office = Office.find_by(id: session[:current_office_id])
      save = true
      if  office && (office.company_id == current_user.company_id)
        map = JSON.parse(params[:map_JSON])
        begin
          walls_backup = Wall.where("office_id = ?", office.id)
          walls_backup.each do |wall|
            Wall.destroy(wall.id)
          end
          map['office']['walls'].each do |wall|
            Wall.create(ax: wall['s_x'], ay: wall['s_y'], cx: wall['e_x'], cy: wall['e_y'], office_id: session[:current_office_id])
          end
        rescue
          walls_new = Wall.where("office_id = ?", office.id)
          walls_new.each do |wall|
            Wall.destroy(wall.id)
          end
          walls_backup.each do |wall|
            Wall.create(ax: wall['s_x'], ay: wall['s_y'], cx: wall['e_x'], cy: wall['e_y'], office_id: session[:current_office_id])
          end
          save = false
        end
        map['office']['tables'].each do |table|
          begin
           if  table['id'] != 0
             db_table = OfficeTable.find_by(id: table['id'], office_id: session[:current_office_id])
             db_table.update(ax: table['s_x'], ay: table['s_y'], length: table['length'], height: table['height'], side: table['side'])
           else
             db_table = OfficeTable.new(ax: table['s_x'], ay: table['s_y'], length: table['length'], height: table['height'], side: table['side'], office_id: session[:current_office_id])
             db_table.save
           end
          rescue
            save = false
          end
        end
        map['office']['points'].each do |point|
          begin
            if  point['id'] != 0
              db_point = Mark.find_by(id: point['id'], office_id: session[:current_office_id])
              db_point.update(ax: point['s_x'], ay: point['s_y'])
            else
              db_point = Mark.new(ax: point['s_x'], ay: point['s_y'], name: point['name'], office_id: session[:current_office_id])
              db_point.save
            end
          rescue
            save = false
          end
        end
      else
        save = false
      end
      render json: "{\"save\":\"#{save}\"}"
    else
      render template: "pages/index"
    end
  end

  def search_office
    if session[:current_user_id]
      current_user = User.find_by(id: session[:current_user_id])
      @offices = Office.where("company_id = ? and name = ?", current_user.company_id, params[:search_office][:name])
      @offices ||= []
      @add = false
      @open = true
      render template: "#{current_user.role}"
    else
      render template: "pages/index"
    end
  end

  def all_offices
    if session[:current_user_id]
      current_user = User.find_by(id: session[:current_user_id])
      @offices = Office.where("company_id = ?", current_user.company_id)
      @offices ||= []
      @add = false
      @open = true
      render template: "#{current_user.role}"
    else
      render template: "pages/index"
    end
  end

  def add_office
    if session[:current_user_id]
    current_user = User.find_by(id: session[:current_user_id])
    begin
      office = Office.new(company_id: current_user.company_id, name: params[:add_office][:name], created_by: current_user.login)
      office.save
      @success = 1
    rescue
      @success = 0
    end
    @offices ||= Office.where("company_id = ?", current_user.company_id)
    @offices||=[]
    @success == 1? (
        @add = false
        session[:current_office_id] = office.id
        session[:current_office_name] = office.name
        @new = true
    ) : @add = true
    @open = false
    render template: "#{current_user.role}"
    else
      render template: "pages/index"
    end
  end

  def manage_office
    if session[:current_user_id]
      current_user = User.find_by(id: session[:current_user_id])
      begin
        company = Office.update(name: params[:manage_office][:name])
        company.save
        @success = 1
      rescue
        @success = 0
      end
      @offices ||= Office.where("company_id = ?", current_user.company_id)
      @offices||=[]
      @add = true
      @open = false
      render template: "#{current_user.role}"
    else
      render template: "pages/index"
    end
  end

  def open_map
    if session[:current_user_id]
      current_user = User.find_by(id: session[:current_user_id])
      office = Office.find_by(id: params[:id])
      if current_user.company_id == office.company_id
        walls = Wall.where("office_id = ?", office.id)
        tables = OfficeTable.where("office_id = ?", office.id)
        points = Mark.where("office_id = ?", office.id)
        p points
        walls ||= []
        tables ||= []
        points ||= []
        map = {office: {walls: walls, tables: tables, points: points}}
        session[:current_office_id] = office.id
        session[:current_office_name] = office.name
        render json: map.to_json, head: 'OK'
      else
        self.index
      end
    else
      render template: "pages/index"
    end
  end

  def manage_personal
    if session[:current_user_id]
      begin
        current_user_data = UserDatum.find_by(id: params[:user].keys[0])
        position = Position.find_by(name: params[:manage_personal][:position])
        unless position
          position = Position.new(name: params[:manage_personal][:position])
          position.save
        end
        current_user_data.update(name: params[:manage_personal][:name], surname: params[:manage_personal][:surname], position_id: position.id)
        workstatus = WorkStatus.find_by("personal_id = ? and dt = CURDATE()", current_user_data.id)
        status_hash = {
            '1'=>'w',
            '2'=>'i',
            '3'=>'v',
            '4'=>'u',
            '5'=>'o'
        }
        if workstatus
          workstatus.update(workstatus: status_hash[params[:work_status]])
        else
          workstatus = WorkStatus.new(personal_id: current_user_data.id, workstatus: status_hash[params[:work_status]], dt: Date.today)
          workstatus.save
        end
        @success = 1
      rescue
        @success = 0
      end
      self.index
    else
      render template: "pages/index"
    end
  end

  def search_personal
    if session[:current_user_id]
      @users = []
      @user_data = {}
      current_user = User.find_by(id: session[:current_user_id])
      if params[:search_personal][:id] && !params[:search_personal][:id].empty?
        search_result = User.find_by(id: params[:search_personal][:id])
        @users.push search_result if search_result
        data_result = UserDatum.find_by(id: params[:search_personal][:id])
        @user_data[data_result.id] = data_result if data_result
      else
        if params[:search_personal][:login] && !params[:search_personal][:login].empty?
          search_result = User.find_by(login: params[:search_personal][:login])
          if search_result
            @users.push search_result
            data_result = UserDatum.find_by(id: search_result.id)
            @user_data[data_result.id] = data_result if data_result
          end
        else
          where_statement = ""
          where_statement += "name = '#{params[:search_personal][:name]}'" if params[:search_personal][:name] && !params[:search_personal][:name].empty?
          if params[:search_personal][:surname] && !params[:search_personal][:surname].empty?
            where_statement += " and " unless where_statement.empty?
            where_statement += "surname = '#{params[:search_personal][:surname]}'"
          end
          if params[:search_personal][:position] && !params[:search_personal][:position].empty?
            if params[:search_personal][:position] == ' '
              tmp = ''
            else
              tmp = params[:search_personal][:position]
            end
            position = Position.find_by(name: tmp)
            where_statement += " and " unless where_statement.empty?
            where_statement += "position_id = #{position.id}"
          end
          search_result = UserDatum.where(where_statement)
          id_arr = []
          search_result.each do |result|
            id_arr.push result.id
            @user_data[result.id] = result
          end
          where_statement = ""
          where_statement = "id in (#{id_arr.join(', ')})" if id_arr.size > 0
          where_statement += ' and ' unless where_statement.empty?
          current_user = User.find_by(id: session[:current_user_id])
          where_statement += "company_id = #{current_user.company_id}"
          user_search = User.where(where_statement)
          if params[:search_personal][:isactive] && !params[:search_personal][:isactive].empty?
            user_search.each do |result|
              @users.push result if result.isactive == params[:search_personal][:isactive]
            end
          else
            user_search.each do |result|
              @users.push result
            end
          end
        end
      end
      @user_data = {}
      @workstatuses = {}
      back_status_hash = {
          'w'=>'1',
          'i'=>'2',
          'v'=>'3',
          'u'=>'4',
          'o'=>'5'
      }
      workstatuses_arr = [['работает', 1], ['болен', 2], ['отпуск', 3],['отгул', 4], ['прочее', 5]]
      @users.each do |user|
        @user_data[user.id] = UserDatum.find_by(id: user.id)
        if @user_data[user.id]['position_id']
          @user_data["#{user.id}_p"] ||= ''
          @user_data["#{user.id}_p"] = (Position.find_by_id(@user_data[user.id]['position_id'])).name
        end
        tmp = WorkStatus.find_by("personal_id = ? and dt = CURDATE()", user.id) ? WorkStatus.find_by("personal_id = ? and dt = CURDATE()", user.id).workstatus : 'w'
        workstatus_tmp = back_status_hash[tmp]
        @workstatuses[user.id] ||= []
        workstatuses_arr.each do |workstatus|
          if workstatus[1] == workstatus_tmp.to_i
            @workstatuses[user.id].unshift workstatus
          else
            @workstatuses[user.id]. push workstatus
          end
        end
      end
      render template: "#{current_user.role}"
    else
      render template: "pages/index"
    end
  end

  def add_personal
    if session[:current_user_id]
      begin
        current_user = User.find_by(id: session[:current_user_id])
        self.registration(params[:add_personal][:login], params[:add_personal][:pwd], 'user')
        new_user = User.find_by(login: params[:add_personal][:login], password: params[:add_personal][:pwd])
        current_user_data = UserDatum.find_by(id: new_user.id)
        new_user.update(company_id: current_user.company_id) if new_user
        new_user ? @success = 1: @success = 0
        position = Position.find_by(name: params[:add_personal][:position])
        unless position
          position = Position.new(name: params[:add_personal][:position])
          position.save
        end
        current_user_data.update(name: params[:add_personal][:name], surname: params[:add_personal][:surname], position_id: position.id)
      rescue
        @success = 0
      end
      self.index
    else
      render template: "pages/index"
    end
  end

  def hr_statistic
    if session[:current_user_id]
      begin
        current_user = User.find_by(id: session[:current_user_id])
        unless current_user
          session[:current_user_id] = nil
          self.index
        else
          if current_user.role == 'hr'
            @users = User.where("company_id = ?", current_user.company_id)
            @user_data = {}
            @workstatuses = {}
            status_arr = %w(
                w
                i
                v
                u
                o
                a
            )
            status_arr.each do |status|
              @workstatuses[status] ||= []
            end
            id_arr = []
            @users.each do |user|
              id_arr.push user.id
            end
            if @users.size > 0
              status_arr.each do |status|
                @workstatuses[status] =  (WorkStatus.where("personal_id in (#{id_arr.join(', ')}) and workstatus = '#{status}'").group(:workstatus, :dt).count.map{|item| {x: item[0][1], y: item[1]}}).sort_by { |key, v| v }.to_json
              end
            end
            @workstatuses['a'] =  (WorkStatus.where("personal_id in (#{id_arr.join(', ')})").group(:dt).count.map{|item| {x: item[0], y: item[1]}}).sort_by { |key, v| v }.to_json
            render template: "#{current_user.role}_statistic"
          else
            session[:current_office_id] = nil
            session[:current_office_name] = nil
            render template: "#{current_user.role}"
          end
        end
        # rescue
        #   session[:current_user_id] = nil
        #   self.index
      end
    else
      render template: "pages/index"
    end
  end

  def offices
    if session[:current_user_id]
      begin
        current_user = User.find_by(id: session[:current_user_id])
        offices ||= Office.where("company_id = ?", current_user.company_id)
        @offices||=[]
        offices.each do |office|
          @offices.push office if office.lat && office.lon
        end
        unless current_user
          session[:current_user_id] = nil
          self.index
        else
          render template: "offices"
        end
        # rescue
        #   session[:current_user_id] = nil
        #   self.index
      end
    else
      render template: "pages/index"
    end
  end

end
