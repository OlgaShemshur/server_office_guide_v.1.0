json.extract! mark, :id, :title, :body, :created_at, :updated_at
json.url mark_url(mark, format: :json)
