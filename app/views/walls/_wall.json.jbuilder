json.extract! wall, :id, :title, :body, :created_at, :updated_at
json.url wall_url(wall, format: :json)
