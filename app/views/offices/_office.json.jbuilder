json.extract! office, :id, :title, :body, :created_at, :updated_at
json.url office_url(office, format: :json)
