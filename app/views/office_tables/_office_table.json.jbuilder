json.extract! office_table, :id, :title, :body, :created_at, :updated_at
json.url office_table_url(office_table, format: :json)
