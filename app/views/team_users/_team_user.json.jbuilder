json.extract! team_user, :id, :title, :body, :created_at, :updated_at
json.url team_user_url(team_user, format: :json)
