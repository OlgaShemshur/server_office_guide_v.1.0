json.extract! user_datum, :id, :title, :body, :created_at, :updated_at
json.url user_datum_url(user_datum, format: :json)
