json.extract! app, :id, :bin, :config, :config.ru, :db, :db_generation_sql_script.sql, :Gemfile, :Gemfile.lock, :lib, :log, :package.json, :public, :Rakefile, :README.md, :storage, :test, :tmp, :vendor, :title, :body, :created_at, :updated_at
json.url app_url(app, format: :json)
