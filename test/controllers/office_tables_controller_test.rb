require 'test_helper'

class OfficeTablesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @office_table = office_tables(:one)
  end

  test "should get index" do
    get office_tables_url
    assert_response :success
  end

  test "should get new" do
    get new_office_table_url
    assert_response :success
  end

  test "should create office_table" do
    assert_difference('OfficeTable.count') do
      post office_tables_url, params: { office_table: { body: @office_table.body, title: @office_table.title } }
    end

    assert_redirected_to office_table_url(OfficeTable.last)
  end

  test "should show office_table" do
    get office_table_url(@office_table)
    assert_response :success
  end

  test "should get edit" do
    get edit_office_table_url(@office_table)
    assert_response :success
  end

  test "should update office_table" do
    patch office_table_url(@office_table), params: { office_table: { body: @office_table.body, title: @office_table.title } }
    assert_redirected_to office_table_url(@office_table)
  end

  test "should destroy office_table" do
    assert_difference('OfficeTable.count', -1) do
      delete office_table_url(@office_table)
    end

    assert_redirected_to office_tables_url
  end
end
