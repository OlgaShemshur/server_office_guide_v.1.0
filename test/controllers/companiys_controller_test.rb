require 'test_helper'

class CompaniysControllerTest < ActionDispatch::IntegrationTest
  setup do
    @companiy = companiys(:one)
  end

  test "should get index" do
    get companiys_url
    assert_response :success
  end

  test "should get new" do
    get new_companiy_url
    assert_response :success
  end

  test "should create companiy" do
    assert_difference('Companiy.count') do
      post companiys_url, params: { companiy: { body: @companiy.body, title: @companiy.title } }
    end

    assert_redirected_to companiy_url(Companiy.last)
  end

  test "should show companiy" do
    get companiy_url(@companiy)
    assert_response :success
  end

  test "should get edit" do
    get edit_companiy_url(@companiy)
    assert_response :success
  end

  test "should update companiy" do
    patch companiy_url(@companiy), params: { companiy: { body: @companiy.body, title: @companiy.title } }
    assert_redirected_to companiy_url(@companiy)
  end

  test "should destroy companiy" do
    assert_difference('Companiy.count', -1) do
      delete companiy_url(@companiy)
    end

    assert_redirected_to companiys_url
  end
end
