require "application_system_test_case"

class TeamUsersTest < ApplicationSystemTestCase
  setup do
    @team_user = team_users(:one)
  end

  test "visiting the index" do
    visit team_users_url
    assert_selector "h1", text: "Team Users"
  end

  test "creating a Team user" do
    visit team_users_url
    click_on "New Team User"

    fill_in "Body", with: @team_user.body
    fill_in "Title", with: @team_user.title
    click_on "Create Team user"

    assert_text "Team user was successfully created"
    click_on "Back"
  end

  test "updating a Team user" do
    visit team_users_url
    click_on "Edit", match: :first

    fill_in "Body", with: @team_user.body
    fill_in "Title", with: @team_user.title
    click_on "Update Team user"

    assert_text "Team user was successfully updated"
    click_on "Back"
  end

  test "destroying a Team user" do
    visit team_users_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Team user was successfully destroyed"
  end
end
