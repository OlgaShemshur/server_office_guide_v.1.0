require "application_system_test_case"

class CompaniysTest < ApplicationSystemTestCase
  setup do
    @companiy = companiys(:one)
  end

  test "visiting the index" do
    visit companiys_url
    assert_selector "h1", text: "Companiys"
  end

  test "creating a Companiy" do
    visit companiys_url
    click_on "New Companiy"

    fill_in "Body", with: @companiy.body
    fill_in "Title", with: @companiy.title
    click_on "Create Companiy"

    assert_text "Companiy was successfully created"
    click_on "Back"
  end

  test "updating a Companiy" do
    visit companiys_url
    click_on "Edit", match: :first

    fill_in "Body", with: @companiy.body
    fill_in "Title", with: @companiy.title
    click_on "Update Companiy"

    assert_text "Companiy was successfully updated"
    click_on "Back"
  end

  test "destroying a Companiy" do
    visit companiys_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Companiy was successfully destroyed"
  end
end
