require "application_system_test_case"

class AppsTest < ApplicationSystemTestCase
  setup do
    @app = apps(:one)
  end

  test "visiting the index" do
    visit apps_url
    assert_selector "h1", text: "Apps"
  end

  test "creating a App" do
    visit apps_url
    click_on "New App"

    fill_in "Gemfile", with: @app.Gemfile
    fill_in "Gemfile.Lock", with: @app.Gemfile.lock
    fill_in "Readme.Md", with: @app.README.md
    fill_in "Rakefile", with: @app.Rakefile
    fill_in "Bin", with: @app.bin
    fill_in "Body", with: @app.body
    fill_in "Config", with: @app.config
    fill_in "Config.Ru", with: @app.config.ru
    fill_in "Db", with: @app.db
    fill_in "Db Generation Sql Script.Sql", with: @app.db_generation_sql_script.sql
    fill_in "Lib", with: @app.lib
    fill_in "Log", with: @app.log
    fill_in "Package.Json", with: @app.package.json
    fill_in "Public", with: @app.public
    fill_in "Storage", with: @app.storage
    fill_in "Test", with: @app.test
    fill_in "Title", with: @app.title
    fill_in "Tmp", with: @app.tmp
    fill_in "Vendor", with: @app.vendor
    click_on "Create App"

    assert_text "App was successfully created"
    click_on "Back"
  end

  test "updating a App" do
    visit apps_url
    click_on "Edit", match: :first

    fill_in "Gemfile", with: @app.Gemfile
    fill_in "Gemfile.Lock", with: @app.Gemfile.lock
    fill_in "Readme.Md", with: @app.README.md
    fill_in "Rakefile", with: @app.Rakefile
    fill_in "Bin", with: @app.bin
    fill_in "Body", with: @app.body
    fill_in "Config", with: @app.config
    fill_in "Config.Ru", with: @app.config.ru
    fill_in "Db", with: @app.db
    fill_in "Db Generation Sql Script.Sql", with: @app.db_generation_sql_script.sql
    fill_in "Lib", with: @app.lib
    fill_in "Log", with: @app.log
    fill_in "Package.Json", with: @app.package.json
    fill_in "Public", with: @app.public
    fill_in "Storage", with: @app.storage
    fill_in "Test", with: @app.test
    fill_in "Title", with: @app.title
    fill_in "Tmp", with: @app.tmp
    fill_in "Vendor", with: @app.vendor
    click_on "Update App"

    assert_text "App was successfully updated"
    click_on "Back"
  end

  test "destroying a App" do
    visit apps_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "App was successfully destroyed"
  end
end
