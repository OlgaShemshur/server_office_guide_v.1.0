require "application_system_test_case"

class OfficeTablesTest < ApplicationSystemTestCase
  setup do
    @office_table = office_tables(:one)
  end

  test "visiting the index" do
    visit office_tables_url
    assert_selector "h1", text: "Office Tables"
  end

  test "creating a Office table" do
    visit office_tables_url
    click_on "New Office Table"

    fill_in "Body", with: @office_table.body
    fill_in "Title", with: @office_table.title
    click_on "Create Office table"

    assert_text "Office table was successfully created"
    click_on "Back"
  end

  test "updating a Office table" do
    visit office_tables_url
    click_on "Edit", match: :first

    fill_in "Body", with: @office_table.body
    fill_in "Title", with: @office_table.title
    click_on "Update Office table"

    assert_text "Office table was successfully updated"
    click_on "Back"
  end

  test "destroying a Office table" do
    visit office_tables_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Office table was successfully destroyed"
  end
end
