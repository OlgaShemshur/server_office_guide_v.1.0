Rails.application.routes.draw do

  resources :users
  resources :wall
  resources :user_data
  resources :team_user
  resources :team
  resources :position
  resources :office_table
  resources :office
  resources :mark
  resources :role
  resources :user
  resources :company
  match "/css/:page", to: 'application#css', via: [:get]
  match "/js/:page", to: 'application#js', via: [:get]
  match "/js/lib/:page", to: 'application#js', via: [:get]
  match "/fonts/:page", to: 'application#fonts', via: [:get]
  match "/hr_statistic", to: 'application#hr_statistic', via: [:get]
  match "/offices", to: 'application#offices', via: [:get]
  match "/login", to: 'application#login', via: [:post]
  match "/registration", to: 'application#company_registration', via: [:post]
  match "/manage", to: 'application#company_manage', via: [:post]
  match "/manage_user", to: 'application#user_manage', via: [:post]
  match "/search_company", to: 'application#search_company', via: [:post]
  match "/search_user", to: 'application#search_user', via: [:post]
  match "/all_company", to: 'application#index', via: [:post]
  match "/add_user", to: 'application#add_user', via: [:post]
  match "/manage_office", to: 'application#manage_office', via: [:post]
  match "/search_office", to: 'application#search_office', via: [:post]
  match "/all_offices", to: 'application#all_offices', via: [:post]
  match "/add_office", to: 'application#add_office', via: [:post]
  match "/open_map", to: 'application#open_map', via: [:post]
  match "/save_map", to: 'application#save_map', via: [:post]
  match "/manage_personal", to: 'application#manage_personal', via: [:post]
  match "/search_personal", to: 'application#search_personal', via: [:post]
  match "/search_personal", to: 'application#search_personal', via: [:post]
  match "/add_personal", to: 'application#add_personal', via: [:post]

  match "/save_map", to: 'application#save_map', via: [:post]
  match "/exit", to: 'application#exit', via: [:get]
  match "/img/:page", to: 'application#img', via: [:get]
  match "/:page", to: 'application#main', via: [:get]
  root 'application#index'

end
